function New-Agent(
    [string]$AgentDirectory,
    [string]$AzureDevOpsAgentTempDirectory
) {
    #Guard out if the directoy already exists
    Write-Verbose "Checking for existance of $AgentDirectory"
    $configFile = Join-Path -Path $AgentDirectory -ChildPath "config.cmd"
    if (Test-Path -Path $AgentDirectory) {
        Write-Host -ForegroundColor Yellow "Agent directory $AgentDirectory already created. Running remove command"
        Start-Process -WorkingDirectory $AgentDirectory -FilePath $configFile -ArgumentList @("remove", "--unattended", "--url $AzureDevOpsUri", "--auth PAT", "--token $AzureDevOpsToken") -Wait -NoNewWindow
    }
    else {
        Write-Host -ForegroundColor Green "Creating Agent Directory $AgentDirectory"
        # Mirror, No files or directories listed (make the copy much faster)
        Robocopy /mir /NFL /NDL $AzureDevOpsAgentTempDirectory $AgentDirectory
    }

    # Convention that is parsable
    $AzureDevOpsAgentName = $(Split-Path -Leaf $AgentDirectory)

    Start-Process -WorkingDirectory $AgentDirectory -FilePath $configFile -ArgumentList @("--unattended", "--url $AzureDevOpsUri", "--auth PAT", "--token $AzureDevOpsToken", "--pool $AzureDevOpsAgentPoolName", "--agent $AzureDevOpsAgentName", "--acceptTeeEula", "--runAsService") -Wait -NoNewWindow
}
