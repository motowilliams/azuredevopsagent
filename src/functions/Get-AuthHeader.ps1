function Get-AuthHeader(
    [string]$Token
) {
    Write-Verbose "Creating authorization header for $Token"
    return @{Authorization = ("Basic {0}" -f ([Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $Token, $Token))))) }
}
