function Get-Agents(
    [System.Uri]$BaseUri,
    [string]$AgentPoolId,
    [string]$Token
) {
    $uri = Join-Uri -uri $baseUri -childPath "_apis/distributedtask/pools/$AgentPoolId/agents?api-version=5.1-preview.1"
    Write-Verbose "poolsUri: $uri"
    $response = (Invoke-RestMethod -Headers (Get-AuthHeader -token $Token) -Uri $uri).value
    return $response
}
