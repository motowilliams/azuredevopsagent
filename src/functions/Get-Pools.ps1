function Get-Pools(
    [System.Uri]$BaseUri,
    [string]$Token
) {
    $uri = Join-Uri -uri $baseUri -childPath "_apis/distributedtask/pools?api-version=5.1-preview.1"
    Write-Verbose "Getting a list of agent pools from $uri"
    $response = Invoke-RestMethod -Headers (Get-AuthHeader -token $Token) -Uri $uri
    return $response.value
}
