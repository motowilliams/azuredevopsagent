function Get-AgentName(
    [string]$AgentDirectory
) {
    Write-Verbose "Calculating agent name from machine name $($env:COMPUTERNAME) and directory name $($AgentDirectory)"
    return "$($env:COMPUTERNAME)-$(Split-Path -Leaf $AgentDirectory)"
}
