function Get-LatestAgentUri (
    [ValidateSet("win-x64", "win-x86", "osx-x64", "linux-x64", "linux-arm", "rhel.6-x64")]
    [string]$AgentDevOpsAgentPlatform
) {
    Write-Verbose "Calling GitHub for lastest release version"
    $latestResponse = Invoke-RestMethod "https://api.github.com/repos/microsoft/azure-pipelines-agent/releases/latest"
    Write-Verbose "Latest version is $($latestResponse.name)"
    $version = $latestResponse.name -replace "v", ""
    Write-Verbose "Calling GitHub for lastest release asset list"
    $assets_url = Invoke-RestMethod $latestResponse.assets_url
    Write-Verbose "Latest asset version is at $($assets_url)"
    Write-Verbose "Calling GitHub for lastest release assets url list"
    $browser_download_urlResponse = Invoke-RestMethod $assets_url.browser_download_url
    [System.Uri]$uriResult = $browser_download_urlResponse | Where-Object { $_.platform -eq $AgentDevOpsAgentPlatform } | Select-Object -ExpandProperty downloadUrl
    return @{ "uri" = ($uriResult.AbsoluteUri); "version" = $version; "filename" = ($uriResult.Segments | Select-Object -Last 1) }
}
