function Get-ArrayItemByIndex(
    [string]$Message,
    [string[]] $Items
) {
    Write-Verbose "Building input for $($Items.length) items"
    Write-Host "$Message"
    $Items | Select-Object -Unique | Foreach-Object {
        $i = $([int][array]::IndexOf($Items, $_) + 1).ToString().PadRight($Items.Length.ToString().Length)
        Write-Host " #$i $($_)"
    }
    $range = 1..$Items.Length
    $ItemIndex = 0
    do {
        $ItemIndex = Read-Host -Prompt "Select number between 1 and $($Items.length) from list"
        if ($ItemIndex -lt 0) {
            $ItemIndex = 0
        }
    } until ($range.Contains([int]$ItemIndex))

    return $Items[$ItemIndex - 1]
}
