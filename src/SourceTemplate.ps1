<#
.SYNOPSIS
    Download, unpackage and configure (1:n) Azure DevOps Agent(s)
.DESCRIPTION
    This script will search GitHub releases for the latest version of the Azure DevOps (VSTS) Agent.
    It will download the latest version of the release from the Azure CDN and unzip it into a _agent directory.
    From there it will robocopy the _agent directory to it's final destination directory with a zero padded directory suffix
    Lastly it will run the configuration batch script to link the agent to the provied Azure DevOps (VSTS) account.
.EXAMPLE
    PS C:\> .\New-AzureDevOpsWindowsAgents.ps1 -AzureDevOpsUri "https://dev.azure.com/your-org-name" -AzureDevOpsToken "your-personal-access-token" -AzureDevOpsAgentPoolName "sample-pool" -AgentCount 3
    This example will download, unpack and configure 3 agents linked to the agent pool called sample-pool.
.PARAMETER AzureDevOpsUri
The Uri to your Azure DevOps instance, such as "https://dev.azure.com/example"
.PARAMETER AzureDevOpsToken
The Personal Access Token (PAT) to be used to configure the Azure DevOps Agent.
https://docs.microsoft.com/en-us/azure/devops/organizations/accounts/use-personal-access-tokens-to-authenticate?view=azure-devops
.PARAMETER AzureDevOpsAgentPoolName
The agent pool name that has been created for the agent(s)
.PARAMETER AgentCount
The number of agents to install and configure
.PARAMETER AzureDevOpsAgentNamePartial
The string token (slug) to use as part of the agent name. The convention is {COMPUTERNAME}-{AzureDevOpsAgentNamePartial}{AgentCount}
.PARAMETER AgentDevOpsAgentPlatform
The platform to download and configure, window, linux or OSX
#>
[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [string]$AzureDevOpsUri,
    [Parameter(Mandatory = $false)]
    [string]$AzureDevOpsToken,
    [Parameter(Mandatory = $false)]
    [string]$AzureDevOpsAgentPoolName,
    [int]$AgentCount = 1,
    [string]$AzureDevOpsAgentNamePartial = "agent",
    [ValidateSet("win-x64", "win-x86", "osx-x64", "linux-x64", "linux-arm", "rhel.6-x64")]
    [string]$AgentDevOpsAgentPlatform = "win-x64"
)

$verbose = ($null -ne $PSCmdlet.MyInvocation.BoundParameters -and $PSCmdlet.MyInvocation.BoundParameters.ContainsKey("Verbose") -and $PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent)
#{SHATEMPLATE}#
#{VERSIONTEMPLATE}#
#{FUNCTIONTEMPLATE}#
$executionDirectory = if (($PSScriptRoot -eq $null) -or ($PSScriptRoot -eq "")) { Get-Location } else { $PSScriptRoot }
$workingDirectory = $executionDirectory

if (!$AzureDevOpsUri -or !$AzureDevOpsToken -or !$AzureDevOpsAgentPoolName) {

    Write-Host -ForegroundColor Green "********************************************************"
    Write-Host -ForegroundColor Green " Azure DevOps Agent Install"
    Write-Host -ForegroundColor Green " PowerShell Edition"
    Write-Host -ForegroundColor Green " Version: $scriptVersion"
    Write-Host -ForegroundColor Green " Commit: $sha"
    Write-Host -ForegroundColor Green "********************************************************"

    $workingDirectory = Read-Host -Prompt "Enter directory location for root agent install (default: $executionDirectory)"
    if (!($workingDirectory)) {
        $workingDirectory = $executionDirectory
    }
    Write-Verbose "Setting working directory to $workingDirectory"

    $AzureDevOpsOrgName = Read-Host -Prompt "Enter Azure DevOps organization (account) name"
    Write-Verbose "Setting Azure Dev Ops Org Name to $AzureDevOpsOrgName"

    $AzureDevOpsUri = Join-Uri -uri "https://dev.azure.com" -childPath $AzureDevOpsOrgName
    Write-Verbose "Setting Azure Dev Ops Uri to $AzureDevOpsUri"

    $SecureString = Read-Host -Prompt "Enter Azure DevOps personal access token (PAT)" -AsSecureString
    $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecureString)
    $AzureDevOpsToken = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
    Write-Verbose "Setting Azure Dev Ops Token (PAT) to $AzureDevOpsToken"

    $allAgentPools = (Get-Pools -BaseUri $AzureDevOpsUri -Token $AzureDevOpsToken)

    $AzureDevOpsAgentPoolName = Get-ArrayItemByIndex -Message "Enter Azure DevOps agent pool name" -Items ($allAgentPools | Select-Object -ExpandProperty name)
    Write-Verbose "Setting Azure Dev Ops Agent Pool Name to $AzureDevOpsAgentPoolName"

    $AgentCount = Read-Host -Prompt "Enter number of agents to install and add to the $AzureDevOpsAgentPoolName pool"
    Write-Verbose "Setting Azure Dev Ops Agent Count to $AgentCount"

    $platform = @("win-x64", "win-x86", "osx-x64", "linux-x64", "linux-arm", "rhel.6-x64")
    $AgentDevOpsAgentPlatform = Get-ArrayItemByIndex -Items $platform -Message "Select platform type to install"
    Write-Verbose "Setting Azure Dev Ops Agent Platform to $AgentDevOpsAgentPlatform"
}

if ($null -eq $allAgentPools) {
    $allAgentPools = (Get-Pools -BaseUri $AzureDevOpsUri -Token $AzureDevOpsToken)
}

$uriResult = Get-LatestAgentUri -AgentDevOpsAgentPlatform $AgentDevOpsAgentPlatform
$packageFilePath = Join-Path -Path $workingDirectory -ChildPath $uriResult.filename
$AzureDevOpsAgentTempDirectory = Join-Path -Path $workingDirectory -ChildPath "_$AzureDevOpsAgentNamePartial"

Write-Verbose "Checking to see if agent pool $($AzureDevOpsAgentPoolName) exists"
$agentPool = $allAgentPools | Where-Object { $_.Name -eq $AzureDevOpsAgentPoolName } | Select-Object -First 1
if ($null -eq $agentPool) {
    Write-Error -Message "Agent pool $($AzureDevOpsAgentPoolName) does not exist. Visit $($AzureDevOpsUri)/_settings/agentpools?poolId=8&_a=agents to create the pool"
    return
}

$agents = Get-Agents -BaseUri $AzureDevOpsUri -AgentPoolId $agentPool.Id -Token $AzureDevOpsToken

# Conditionally download the agent package from MS
if (!(Test-Path -Path $packageFilePath)) {
    Write-Host -ForegroundColor Green "Downloading $packageFilePath file" -NoNewline
    (New-Object System.Net.WebClient).DownloadFile(($uriResult.uri), $packageFilePath)
    Unblock-File -Path $packageFilePath
    Write-Host -ForegroundColor Green " done."
}
else {
    Write-Host -ForegroundColor Green "$packageFilePath has already been downloaded"
}

# Conditionally unpack the package
if (!(Test-Path -Path $AzureDevOpsAgentTempDirectory)) {
    Expand-Archive -Path $packageFilePath -DestinationPath $AzureDevOpsAgentTempDirectory
    Set-Content -Value $uriResult.version -Path (Join-Path -Path $AzureDevOpsAgentTempDirectory -ChildPath version)
}

1..$AgentCount | ForEach-Object {
    $count = "$($_)".PadLeft("$AgentCount".Length + 1, "0")

    $agentName = Get-AgentName -AgentDirectory "agent$Count"
    Write-Verbose "Using agent name $agentName"

    if ($agents | Where-Object { $_.Name -eq $agentName } ) {
        Write-Warning "Skipping installation of agent $agentName. It is already configured on the $($AzureDevOpsUri) instance."
        return
    }
    $agentDirectory = (Join-Path -Path $workingDirectory -ChildPath $agentName)
    Write-Verbose "Starting installation to $agentDirectory"
    New-Agent -AgentDirectory $agentDirectory -AzureDevOpsAgentTempDirectory $AzureDevOpsAgentTempDirectory
}
