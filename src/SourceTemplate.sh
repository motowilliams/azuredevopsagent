#!/bin/bash

executionDirectory=$(pwd)

echo -e "\e[92m********************************************************\e[39m"
echo -e "\e[92m Azure DevOps Agent Install\e[39m"
echo -e "\e[92m Bash Edition\e[39m"
echo -e "\e[92m Version: $scriptVersion\e[39m"
echo -e "\e[92m Commit: $sha\e[39m"
echo -e "\e[92m********************************************************\e[39m"

echo -ne "\e[92mEnter directory location for root agent install (default: $executionDirectory): \e[39m"
read workingDirectory

if test -z "$workingDirectory"
then
    workingDirectory=$executionDirectory
fi

echo -e "Base installation directory $workingDirectory"
