#!/usr/bin/env bash

echo "Setting directory to $DOCS_SRC_PATH"
cd $DOCS_SRC_PATH

echo "Building documentation site"
mkdocs serve -v --dev-addr=0.0.0.0:$LOCAL_PORT
