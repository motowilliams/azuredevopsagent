FROM debian:latest

ENV PYTHONUNBUFFERED=1

RUN apt-get update && \
    apt-get install -y -qq \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    gnupg-agent \
    jq \
    lsb-release \
    python3 \
    python3-pip \
    rsync \
    software-properties-common \
    tar \
    wget \
    zip

# Docker apt repo
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
RUN apt-get update && apt-get install -y docker-ce docker-ce-cli

# https://www.mkdocs.org/
RUN pip3 install --no-cache --upgrade pip setuptools
RUN pip3 install mkdocs mkdocs-macros-plugin mkdocs-material

RUN FILE=theme.list && \
    echo 'mkdocs'      >> /$FILE && \
    echo 'readthedocs' >> /$FILE && \
    echo 'material'    >> /$FILE

RUN cd /tmp && \
    git clone https://gitlab.com/motowilliams/mkdocs-macros-file-include.git && \
    cd mkdocs-macros-file-include && \
    python3 setup.py install && \
    cd / && \
    rm -rf /tmp/

ADD docs/docker/build.sh /usr/local/bin/
ADD docs/docker/serve.sh /usr/local/bin/

RUN mv /usr/local/bin/build.sh /usr/local/bin/build && \
    chmod 755 /usr/local/bin/build && \
    mv /usr/local/bin/serve.sh /usr/local/bin/serve && \
    chmod 755 /usr/local/bin/serve

# Install Powershell 7
RUN mkdir -p /tmp
RUN chmod 1777 /tmp
RUN wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb
RUN apt-get update
RUN apt-get install -y powershell
