# {{ config.site_name }}

Azure DevOps Agent installer will automate installing a pipeline agent on the machine executing the script.

All you need if your Azure DevOps instance url, a personal access token (PAT) and a pre-defined agent pool name. Select a number of agents along with a platform moniker and the rest is automatic.

### PowerShell

=== "Execute"

    ``` powershell
    irm {{ config.site_url }} | iex
    ```

=== "Script"

    ```
    {{ include_file('content/New-AzureDevOpsWindowsAgents.ps1', '    ') }}
    ```

<sub><sup>
<a target="_blank" href="https://gitlab.com/motowilliams/azuredevopsagent/-/commit/{{git.short_commit}}">commit: {{ git.short_commit}} ({{ git.date}}) by {{ git.author}} {{ git.message }}</a>
</sup></sub>
