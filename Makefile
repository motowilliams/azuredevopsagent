export DOCS_DIR_NAME ?= content
export DOCS_SRC ?= docs
export PUBLISH_DIR ?= public
export DOCKER_DIR ?= docker
export REPO_URL ?= https://example.com/repo
export SITE_DIR ?= site
export SITE_NAME ?= Azure DevOps Agent Installer
export PROTOCOL ?= http
export DOMAIN_NAME ?= 127.0.0.1:8000
export SITE_URL ?= $(PROTOCOL)://$(DOMAIN_NAME)/
export LOCAL_PORT ?= 8000

.DEFAULT_GOAL := help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

export SHELL := /bin/bash
export BASH_CMD := $(SHELL) -c
export COMMIT_HASH ?= $(shell [ -d ".git" ] && git rev-parse --short HEAD)
export REPO_ROOT := $(shell [[ -z "${CI}" ]] && echo '/app' || echo $(PWD))

export IMAGE_NAME ?= azuredevopsagent
export IMAGE_VERSION ?= latest
export IMAGE_TAG = $(IMAGE_NAME):$(IMAGE_VERSION)
export ADDITIONAL_BUILD_ARGS ?=

export DOCS_SRC_PATH=$(REPO_ROOT)/$(DOCS_SRC)
export PUBLISH_PATH := $(REPO_ROOT)/$(PUBLISH_DIR)

export CONTENT_SITE_PATH=$(DOCS_SRC_PATH)/$(DOCS_DIR_NAME)
export DOCS_SITE_PATH=$(DOCS_SRC_PATH)/$(SITE_DIR)
export DOCKER_PATH=$(DOCS_SRC_PATH)/$(DOCKER_DIR)

ifdef CI
	export IMAGE_TAG=
	export DOCKER_COMMAND :=
else
	export DOCKER_COMMAND := docker run -it \
		-v $(PWD):$(REPO_ROOT) \
		--env CI=TRUE \
		--env COMMIT_HASH=$(COMMIT_HASH) \
		--env CONTENT_SITE_PATH=$(CONTENT_SITE_PATH) \
		--env DOCS_DIR=$(DOCS_DIR_NAME) \
		--env DOCS_SITE_PATH=$(DOCS_SITE_PATH) \
		--env DOCS_SRC_PATH=$(DOCS_SRC_PATH) \
		--env LOCAL_PORT=$(LOCAL_PORT) \
		--env PUBLISH_PATH=$(PUBLISH_PATH) \
		--env REPO_ROOT=$(REPO_ROOT) \
		--env REPO_URL=$(REPO_URL) \
		--env SITE_DIR=$(SITE_DIR) \
		--env SITE_NAME="$(SITE_NAME)" \
		--env SITE_URL=$(SITE_URL) \
		--name $(IMAGE_NAME) \
		--rm \
		-w $(DOCS_SRC_PATH)
endif

clean_docs: ## Removes the content artifacts directory (SITE_DIR) and compressed archive (SITE_DIR.zip)
	$(eval CMD := rm -rf $(SITE_DIR).zip && rm -rf $(SITE_DIR) && rm -rf $(DOCS_SITE_PATH) && rm -rf $(PUBLISH_PATH) )
	@echo "Cleaning $(SITE_DIR).zip & $(SITE_DIR) in $(SITE_DIR)"
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

build_docs: ##clean_docs ## Builds the mkdocs build command to generate content to (SITE_DIR)
	$(eval CMD := build && \
	cp /theme.list $(DOCS_SITE_PATH)/ && \
	cp $(REPO_ROOT)/Makefile $(DOCS_SITE_PATH)/ && \
	cp $(DOCKER_PATH)/Dockerfile $(DOCS_SITE_PATH)/ && \
	cp $(DOCKER_PATH)/build.sh $(DOCS_SITE_PATH)/ && \
	cp $(DOCKER_PATH)/serve.sh $(DOCS_SITE_PATH)/ \
	)
	@echo "Building"
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

publish_docs: ##clean_docs ## Builds the mkdocs build command to generate content to (SITE_DIR)
	$(eval CMD := \
	mkdir -p $(PUBLISH_PATH) && \
	cp -R $(DOCS_SITE_PATH)/* $(PUBLISH_PATH)/ \
	)
	@echo "Publishing"
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

debug: ## Runs the docker image interactively for debugging purposes
	@$(DOCKER_COMMAND) $(IMAGE_TAG) $(CMD)

docker_build: ## Build the docker image used for these make targets
	docker build $(ADDITIONAL_BUILD_ARGS) -t $(IMAGE_TAG) . -f docs/docker/Dockerfile

docker_build_wrangler: ## Build the docker image used for these make targets
	@docker build $(ADDITIONAL_BUILD_ARGS) -t $(IMAGE_TAG) . -f docs/docker/Dockerfile-Wrangler

package_docs: build_docs ## Builds the mkdocs build command to generate content to (SITE_DIR) and creates compressed archive (SITE_DIR.zip)
	$(eval CMD := cd $(DOCS_SRC_PATH) && zip -r ../$(SITE_DIR).zip ./)
	@echo "Packaging"
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

serve_docs:  ## Runs the mkdocs server at 0.0.0.0:$(LOCAL_PORT)
	$(eval CMD := serve)
	$(DOCKER_COMMAND) -p $(LOCAL_PORT):$(LOCAL_PORT) --expose $(LOCAL_PORT) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

print-%: ; @echo $*=$($*)

printenv:
	printenv | sort
