[CmdletBinding()]
param(
    [string]$sha,
    [string]$version,
    [string]$artifactScriptPath = "$PSScriptRoot\artifacts\"
)

function Write-Log (
    [string]$Message
) {
    Write-Host -ForegroundColor Green -Object "[$(Get-Date -Format o)] $Message"
}


$srcDirectory = Join-Path -Path $PSScriptRoot -ChildPath "src"
Write-Log "Set source directory to $srcDirectory"
$functionScriptDirectory = Join-Path -Path $srcDirectory -ChildPath "functions"
Write-Log "Set functions directory to $functionScriptDirectory"

function Set-BlankLine {
    [CmdletBinding()]
    param (
        [parameter(ValueFromPipeline)][string]$TargetString,
        [int]$Count = 1
    )

    return $TargetString.Trim() + "`r`n" * ($Count + 1)
}

function Join-FunctionScripts {
    $functionContent = "# Functions"
    $functionContent += "`r`n"
    Get-ChildItem -Path $functionScriptDirectory | Sort-Object FullName | ForEach-Object {
        Write-Log "Processing $($_)"
        $functionContent += $(Get-Content -Raw -Path $_.FullName) | Set-BlankLine
    }
    return $functionContent
}

function New-CompiledScrpt {
    if (Test-Path -Path $artifactScriptPath) {
        Remove-Item -Force -Recurse -Path $artifactScriptPath
    }
    New-Item -ItemType Directory -Path $artifactScriptPath -Force | Out-Null

    $template = Get-Content (Join-Path -Path $srcDirectory -ChildPath "SourceTemplate.ps1")
    Write-Log "Reading source template"

    Write-Log "Starting compiling function scripts"
    $renderedTemplate = $template.Replace("#{FUNCTIONTEMPLATE}#", (Join-FunctionScripts))
    Write-Log "Done compiling function scripts"

    if ($sha) {
        Write-Log "Starting compiling sha output"
        $renderedTemplate = $renderedTemplate.Replace("#{SHATEMPLATE}#", '$sha=' + "`"$($sha)`"")
        Write-Log "Done compiling sha output"
    }

    if ($version) {
        Write-Log "Starting compiling version output"
        $renderedTemplate = $renderedTemplate.Replace("#{VERSIONTEMPLATE}#", '$scriptVersion=' + "`"$($version)`"")
        Write-Log "Done compiling version output"
    }

    New-Item -ItemType Directory -Force -Path $artifactScriptPath | Out-Null

    # Copy the doc page
    Copy-Item -Recurse -Force -Path "$PSScriptRoot\src\web\*" -Destination $artifactScriptPath

    Write-Log "Writing complete compiled script"
    $compileScriptPath = (Join-Path -Path $artifactScriptPath -ChildPath "New-AzureDevOpsWindowsAgents.ps1")
    Add-Content -Encoding Ascii -Path $compileScriptPath -Value ($renderedTemplate)
}

Write-Log "Starting Build"
New-CompiledScrpt
Write-Log "Finsihed Build"
